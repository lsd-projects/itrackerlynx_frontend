import { Component, OnInit, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AuthService } from "../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username:string;
  password:string;

  constructor(private auth:AuthService) { }

  ngOnInit() {
  }

  public login(){
    this.auth.login(this.username, this.password, this);
  }

  public exeption(err){
    //this.showNotification('top','center', err)
  }

}
