import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  public statusSidebar = true;

  constructor() { }


  public changeSidebar(){
    this.statusSidebar = !this.statusSidebar;
  }
}
