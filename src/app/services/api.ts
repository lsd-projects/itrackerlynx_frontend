import axios from 'axios';

const server = 'http://localhost:3030/api';

export const API = {
  get: path => axios.get(`${server}${path}`)
                    .then(response => response.data),
  put: (path, body) => axios.put(`${server}${path}`, body,{headers: {'Content-Type': 'application/json', 'lsd-access-token': localStorage.getItem('lsd-access-token')}})
                            .then(response => response.data),
  post: (path, body) => axios.post(`${server}${path}`, body, {headers: {'Content-Type': 'application/json', 'lsd-access-token': localStorage.getItem('lsd-access-token')}})
                             .then(response => response.data),
  postAuth: (path, body) => axios.post(`${server}${path}`, body)
                             .then(response => response.data),
};
