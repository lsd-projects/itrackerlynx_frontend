import { Injectable } from '@angular/core';
import { API } from '../services/api';
import { Router } from "@angular/router"

import { LoginComponent } from '../login/login.component';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLogged:boolean;

  constructor(private router: Router) {
    if(localStorage.getItem("lsd-access-token") == null || localStorage.getItem("lsd-access-token") == ""){
      this.isLogged = false
    }else{
      this.isLogged = true
    }
  }


  login(username:string, password:string, loguin:LoginComponent){
    let body = {"username": username,"password": password}

    API.postAuth('/auth', body)
      .then(res => this.loginOk(res.token, username))
      .catch(res => loguin.exeption(res))
  }

  private loginOk(token:string, username:string){
    localStorage.setItem('lsd-access-token',token);
    this.isLogged = true;
    this.getUser(username);

    this.router.navigateByUrl('/dashboard');
  }

  private getUser(username:string){
    let body = {"username": username}

    API.post('/user', body)
      .then(res => this.setUser(res.user))
      .catch(res => console.log(res))
  }

  private setUser(user){
    localStorage.setItem('lsd-user',user.user);
    localStorage.setItem('lsd-img',user.avatar);
    localStorage.setItem('lsd-fname',user.firstName);
    localStorage.setItem('lsd-lname',user.lastName);
    localStorage.setItem('lsd-email',user.email);
  }
}
