import { Component, OnInit } from '@angular/core';

import { SidebarService } from '../../services/sidebar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private side:SidebarService) { }

  ngOnInit() {
  }

  public clickSidebar() {
    this.side.changeSidebar()
  }

  public sidebarStatus(){
    return this.side.statusSidebar
  }

}
