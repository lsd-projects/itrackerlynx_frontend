import { Component } from '@angular/core';
import { AppModule } from './app.module';

import { SidebarService } from './services/sidebar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'iTracker';

  constructor(private side:SidebarService){}

  public sidebarStatus(){
    return this.side.statusSidebar
  }
}
