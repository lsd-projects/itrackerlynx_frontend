import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './partials/navbar/navbar.component';
import { FooterComponent } from './partials/footer/footer.component';
import { SidebarComponent } from './partials/sidebar/sidebar.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MenuteamComponent } from './admin/team/menuteam/menuteam.component';
import { AddteamComponent } from './admin/team/addteam/addteam.component';
import { SecurelinkService } from './services/securelink.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    SidebarComponent,
    DashboardComponent,
    MenuteamComponent,
    AddteamComponent,
  ],
  imports: [
    FormsModule,
    RouterModule,
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  exports: [
    FormsModule,
    RouterModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    SecurelinkService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
