import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SecurelinkService } from './services/securelink.service';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MenuteamComponent } from './admin/team/menuteam/menuteam.component';
import { AddteamComponent } from './admin/team/addteam/addteam.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'team',
    component: MenuteamComponent,
    canActivate: [SecurelinkService],
  },
  {
    path: 'team/addteam',
    component: AddteamComponent,
    canActivate: [SecurelinkService],
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
